# Linode Automation

This project is for automating the deployment of various types of servers to Linode using Terraform configurations and Ansible playbooks.  It requires access to the internal Linode network, so this must be run on either an existing instance on the same Linode account, or a workstation with a VPN connection to the Linode private network of your Linode account.

Terraform and Ansible (the Python 3 version) are required.  Ansible comes with most GNU/Linux distributions.  Terraform can be downloaded from the [Terraform download page](https://www.terraform.io/downloads.html).


## Linode API Token

Terraform requires a v4 API token to access your Linode account.  To get this, login to https://cloud.linode.com and then go to https://cloud.linode.com/profile/tokens.  Click "Add a Personal Access Token".

The old type of token created on www.linode.com will not work.


## Ansible Vault setup

You must have Ansible Vault installed and a password configured.  The password is set by placing it in a file (e.g., $HOME/.ansiblepass) and setting the shell environment variable ANSIBLE\_VAULT\_PASSWORD\_FILE to the path of that file.

```bash
pwgen -sy 16 1 > ~/.ansiblepass
chmod 0600 ~/.ansiblepass
export ANSIBLE_VAULT_PASSWORD_FILE=~/.ansiblepass
```

Note that when using Ansible Vault to encrypt the Terraform secrets, the unencrypted secrets will at times exist in a working file named terraform.tfstate.  This file is in .gitignore to prevent it being added to Git, but it is still important to be aware that the plain-text secrets exist in this file.  In some situations this file existing locally may be an issue, in which case another encryption strategy would be needed.


## Projects

* [Postgres Server](./postgres-server)


## Relevant Documentation

* [Terraform Documentation](https://www.terraform.io/docs/)
* [Ansible Documentation](https://docs.ansible.com/ansible/latest/)
* [Linode Provider for Terraform](https://www.terraform.io/docs/providers/linode/)
* [Linode v4 API](https://developers.linode.com/api/v4)
